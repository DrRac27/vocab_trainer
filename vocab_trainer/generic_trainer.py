from abc import ABC, abstractmethod
import os
import json
import csv
from vocab_trainer.cards import CardBox, Card
from pathlib import Path


class FileManager:
    standard_name: str = "standard"
    folder_name: str = "vocabularies"
    extension: str = ".json"

    def __init__(self):
        self.last_filepath = None

    def _get_box_path(self, name: str = None, full_path: str = None) -> Path:
        if full_path is not None:
            return Path(full_path)

        if name is None:
            return self.last_filepath

        xdg_data_home = os.environ.get("XDG_DATA_HOME") or \
            Path.home() / ".local" / "share"

        return Path(xdg_data_home) / self.app_name / name + self.extension

    def load_box_from_file(
            self,
            filepath: str = None,
            must_exist=True,
            ) -> CardBox:

        if not filepath:
            filepath = self.standard_filepath

        self.last_filepath = filepath

        try:
            box = json.load(open(filepath))
            return CardBox.from_dict(box)
        except FileNotFoundError:
            if must_exist:
                raise
            return CardBox()

    def save_box_to_file(self, box: CardBox, full_path: str = None) -> None:
        box_path = self._get_box_path(full_path=full_path)
        box_path.parent.mkdir(parents=True, exist_ok=True)

        json.dump(box.__dict__(), open(box_path, "w"), indent=4)

        self.last_filepath = box_path

    @property
    def standard_filepath(self) -> str:
        if self.last_filepath:
            return self.last_filepath

        return self._get_box_path(name=self.standard_name)

    @property
    def boxes(self) -> list:
        try:
            ls = os.listdir(os.path.expanduser(self.config_path))
        except FileNotFoundError:
            return list()
        boxes = list()
        for filepath in ls:
            basename = os.path.basename(filepath)
            stem, extension = os.path.splitext(basename)
            if extension == self.extension:
                boxes.append(stem)
        return boxes

    def load_box(self, name: str = None, must_exist=True) -> CardBox:
        return self.load_box_from_file(
            filepath=self._get_box_path(name=name),
            must_exist=must_exist,
        )

    def save_box(self, box: CardBox, name: str = None):
        self.save_box_to_file(
            box=box,
            full_path=self._get_box_path(name=name),
        )


class VocabularyTrainer(ABC):
    def __init__(self):
        self.box = CardBox()
        self.file_manager = FileManager()

    @abstractmethod
    def run(self) -> None:
        pass

    def import_csv(self, filepath: str) -> None:
        def check_header(real_header: list, right_header: list) -> None:
            if len(right_header) != len(real_header):
                raise Exception("Wrong header: lengths don't match")

            for right, real in zip(right_header, real_header):
                if right != real:
                    raise Exception("Wrong header")

        def parse(csv_reader):
            right_header = [
                "front_lang",
                "front_written",
                "front_spoken",
                "back_lang",
                "back_written",
                "back_spoken",
            ]
            check_header(next(csv_reader), right_header)

            for row in csv_reader:
                self.box.add_card(Card(
                    front_language=row[0],
                    front_written=row[1],
                    front_spoken=row[2],
                    back_language=row[3],
                    back_written=row[4],
                    back_spoken=row[5],
                ))

        with open(filepath) as fp:
            csv_reader = csv.reader(fp.readlines())
            parse(csv_reader)


if __name__ == "__main__":
    class Trainer(VocabularyTrainer):
        def run(self):
            pass
    voc = Trainer()
    voc.import_csv("tests/data/test.csv")
    print(voc.box)
