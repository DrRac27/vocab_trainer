from dataclasses import dataclass, field
from time import time
import datetime
import random


@dataclass
class Card:
    front_written: str
    front_language: str
    back_written: str
    back_language: str
    front_spoken: str = None
    back_spoken: str = None
    last_check: float = 0
    level: int = 0

    @classmethod
    def from_dict(cls, d) -> "Card":
        return cls(
            front_written=d["front_written"],
            front_language=d["front_language"],
            back_written=d["back_written"],
            back_language=d["back_language"],
            front_spoken=d.get("front_spoken", Card.front_spoken),
            back_spoken=d.get("back_spoken", Card.back_spoken),
            last_check=d.get("last_check", Card.last_check),
            level=d.get("level", Card.level),
        )

    def pretty_print(self, width: int = 44, front=True) -> None:
        written = self.front_written if front else self.back_written
        spoken = self.front_spoken if front else self.back_spoken
        language = self.front_language if front else self.back_language

        print(width*"#")
        print("#"+(width - 2)*" "+"#")
        print(f"# {written.center(width - 4)} #")
        if spoken:
            spoken = "["+spoken+"]"
            print(f"# {spoken.center(width - 4)} #")
        print(f"# {language.rjust(width - 4)} #")
        print(width*"#")

    def was_checked(self) -> None:
        self.last_check = time()

    def __dict__(self) -> dict:
        return {
            "front_written": self.front_written,
            "front_language": self.front_language,
            "front_spoken": self.front_spoken,
            "back_written": self.back_written,
            "back_language": self.back_language,
            "back_spoken": self.back_spoken,
            "last_check": self.last_check,
            "level": self.level,
        }

    def turned_around(self) -> "Card":
        return Card(
            front_written=self.back_written,
            front_language=self.back_language,
            front_spoken=self.back_spoken,
            back_written=self.front_written,
            back_language=self.front_language,
            back_spoken=self.front_spoken,
            # NOTE: last_check missing intentionally
            level=self.level,
        )


@dataclass
class CardBox:
    cards: list[Card] = field(default_factory=list)
    level_days: list[int] = field(default_factory=lambda: [0, 1, 3, 9, 29, 90, -1])
    last_checked_card: Card = None

    @classmethod
    def from_dict(cls, d) -> "CardBox":
        cards = d.get("cards", CardBox().cards)

        last_checked_card = d.get("last_checked_card", None)
        if last_checked_card is not None:
            last_checked_card = Card.from_dict(last_checked_card)

        return cls(
            cards=[Card.from_dict(c) for c in cards],
            level_days=d.get("level_days", CardBox().level_days),
            last_checked_card=last_checked_card,
        )

    def __dict__(self) -> dict:
        last_checked_card = self.last_checked_card
        if last_checked_card is not None:
            last_checked_card = last_checked_card.__dict__()

        return {
            "cards": [c.__dict__() for c in self.cards],
            "level_days": self.level_days,
            "last_checked_card": last_checked_card,
        }

    @property
    def seconds_till_next_due(self) -> float:
        if not self.cards:
            return None
        lowest_time = self._due_in_seconds(self.cards[0])
        for card in self.cards[1:]:
            if self._due_in_seconds(card) < lowest_time:
                lowest_time = self._due_in_seconds(card)
            if lowest_time == 0:
                break
        return lowest_time

    def _due_in_seconds(self, card: Card) -> float:
        due_in = card.last_check + self._get_show_after_seconds(card) - time()
        if due_in < 0:
            due_in = 0
        return round(due_in)

    @property
    def time_till_next_due(self) -> str:
        secs = self.seconds_till_next_due
        if secs is None:
            return None
        return datetime.timedelta(seconds=secs)

    @property
    def is_empty(self) -> bool:
        return not self.cards

    @property
    def due_cards(self) -> list[Card]:
        due_cards = []
        for card in self.cards:
            if self._is_due(card):
                due_cards.append(card)
        return due_cards

    @property
    def next_due_card(self) -> Card:
        card = random.choice(self.due_cards)
        if len(self.due_cards) > 1 and card == self.last_checked_card:
            card = self.next_due_card
            # TODO: If there are duplicates this can lead to endless loop

        self.last_checked_card = card

        return card

    def _get_show_after_seconds(self, card: Card) -> float:
        # TODO: Find better name
        show_after_days = self.level_days[card.level]
        show_after_seconds = show_after_days * 60 * 60 * 24
        return show_after_seconds

    def _is_due(self, card: Card) -> bool:
        show_after_seconds = self._get_show_after_seconds(card)
        return time() > card.last_check + show_after_seconds

    @property
    def has_due_cards(self) -> bool:
        for card in self.cards:
            if self._is_due(card):
                return True
        return False

    def add_card(
            self,
            card: Card,
            turned_around_also=True,
            ) -> None:

        self.cards.append(card)
        if turned_around_also:
            self.cards.append(card.turned_around())

    def was_right(self, card: Card) -> None:
        if card.level + 1 >= len(self.level_days):
            return  # TODO
            raise Exception("TODO")

        card.level += 1
        card.was_checked()

    def was_wrong(self, card: Card) -> None:
        if card.level == 0:
            return

        card.level -= 1
        card.was_checked()
