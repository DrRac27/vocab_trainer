from argparse import ArgumentParser
from vocab_trainer.generic_trainer import VocabularyTrainer
from vocab_trainer.cards import Card


class CLI_Trainer(VocabularyTrainer):
    def run(self) -> None:
        parser = ArgumentParser()
        parser.add_argument(
            "action",
            type=str,
            choices=[
                "create",
                "learn",
                "show-boxes",
                "show-stats",
                "import",
                "check-due-cards",
            ],
        )
        parser.add_argument(
            "-b", "--box",
            type=str,
            help="Name of the box",
            default=None,
        )
        parser.add_argument(
            "--all-boxes",
            help="Use all boxes for learning",
            action="store_true",
        )
        parser.add_argument(
            "-f", "--box-file",
            type=str,
            help="File of the box",
            default=None,
        )
        parser.add_argument(
            "--csv-file",
            type=str,
            help=(
                "CSV file to import. First line: front_lang,front_written,"
                "front_spoken,back_lang,back_written,back_spoken"
            ),
            default=None,
        )
        parser.add_argument(
            "--front-lang",
            type=str,
            help="Front language (helpful for 'create' and 'import')",
            default=None,
        )
        parser.add_argument(
            "--back-lang",
            type=str,
            help="Back language (helpful for 'create' and 'import')",
            default=None,
        )
        parser.add_argument(
            "--no-front-pronounce",
            help="Don't ask for pronounciation on front card in create/import",
            action="store_true",
        )
        parser.add_argument(
            "--no-back-pronounce",
            help="Don't ask for pronounciation on back card in create/import",
            action="store_true",
        )
        args = parser.parse_args()

        if (
                (args.box and args.box_file) or
                (args.all_boxes and args.box) or
                (args.all_boxes and args.box_file)
        ):

            print("Error: --box, --all-boxes and "
                  "--box-file conflict each other")
            return
        elif args.box:
            self.box = self.file_manager.load_box(args.box, must_exist=False)
            self._run_action(args)
        elif args.all_boxes:
            for b in self.file_manager.boxes:
                print(f">> Loading box '{b}'...")
                self.box = self.file_manager.load_box(b, must_exist=False)
                self._run_action(args)
                print()
        elif args.box_file:
            self.box = self.file_manager.load_box_from_file(
                filepath=args.box_file,
                must_exist=False,
            )
            self._run_action(args)
        else:
            self.box = self.file_manager.load_box(must_exist=False)
            self._run_action(args)

    def _run_action(self, args) -> None:
        if args.action == "create":
            self.create_card(args)
            self.file_manager.save_box(self.box)
        elif args.action == "learn":
            if self.box.is_empty:
                return
            self.learn_card()
            self.file_manager.save_box(self.box)
        elif args.action == "show-boxes":
            self.show_boxes()
        elif args.action == "show-stats":
            self.show_stats()
        elif args.action == "import":
            self.import_(args)
            self.file_manager.save_box(self.box)
        elif args.action == "check-due-cards":
            if args.all_boxes:
                for box in self.file_manager.boxes:
                    b = self.file_manager.load_box(box)
                    if b.has_due_cards:
                        exit(0)
                exit(1)

            exit(0 if self.box.has_due_cards else 1)

    def create_card(self, args) -> None:
        front_written = input("What is written on the front? ")
        front_language = args.front_lang or input(
            "What is the front language? "
        )
        front_spoken = None if args.no_front_pronounce else input(
            "(optional) How to pronounce it: "
        )
        back_written = input("What is written on the back? ")
        back_language = args.back_lang or input("What is the back language? ")
        back_spoken = None if args.no_back_pronounce else input(
            "(optional) How to pronounce it: "
        )
        if front_spoken == "":
            front_spoken = None
        if back_spoken == "":
            back_spoken = None

        self.box.add_card(Card(
            front_written=front_written,
            front_language=front_language,
            front_spoken=front_spoken,
            back_written=back_written,
            back_language=back_language,
            back_spoken=back_spoken,
        ))

    def learn_card(self) -> None:
        def was_right(card):
            self.box.was_right(card)
            print("Congratulations!")

        if not self.box.has_due_cards:
            print("Congratulations! You are up to date.")
            print("Next card is due in", self.box.time_till_next_due)
            return

        card = self.box.next_due_card

        card.pretty_print()
        guess = input(
            "Type your guess if you want, [Enter] shows you the back\n"
        )
        card.pretty_print(front=False)
        if guess == card.back_written:
            was_right(card)
            return

        selection = ""
        while selection != "n" and selection != "y":
            selection = input("Was it right? [yn]")
            if selection == "y":
                was_right(card)
            elif selection == "n":
                self.box.was_wrong(card)
                print("Ok another try then!")
            else:
                print("Invalid answer, please try again")

    def show_stats(self) -> None:
        print(self.box)
        print("Time till next due:", self.box.time_till_next_due)

    def show_boxes(self) -> None:
        print(self.file_manager.boxes)

    def import_(self, args) -> None:
        if args.csv_file:
            self.import_csv(args.csv_file)
        else:
            raise Exception("import needs a source to import from: --csv-file")


def main():
    trainer = CLI_Trainer()
    trainer.run()


if __name__ == "__main__":
    main()
