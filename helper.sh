#!/bin/bash


PYTHON="python3"


parse_options() {
    case "$1" in
        "run")
            shift
            run $@
            ;;
        "dev")
            shift
            dev $@
            ;;
        "lint")
            lint
            ;;
        "tests")
            tests
            ;;
        "clean")
            clean
            ;;
        *)
            usage
            ;;
    esac
}


usage() {
    echo "Usage: $0 ACTION"
    echo ""
    echo "ACTION can be one of:"
    echo "run"
    echo "      Runs this program. You can pass arguments after the 'run'."
    echo "dev"
    echo "      Runs this program with developer settings (arguments allowed)"
    echo "lint"
    echo "      Check the code for style and errors"
    echo "tests"
    echo "      Run the tests (also runs lint)"
    echo "clean"
    echo "      Delete build files"
}


run() {
    $PYTHON -m vocab_trainer.cli_trainer $@
}


dev() {
    run --box-file tests/data/dev.json $@
}


lint() {
    flake8 vocab_trainer tests
}


tests() {
    lint
    pytest
}


clean() {
    rm -rf "$OUTPUT_DIR"
}


parse_options "$@"
